const db = require('firebase-admin').firestore();

exports.createOrder = async (title, bookingDate, address, customer) => {
    await db.collection('orders').add({
        title,
        bookingDate,
        address,
        customer,
    });
};

exports.editOrder = async (orderId, title, bookingDate) => {
    await db
        .collection('orders')
        .doc(orderId)
        .update({ title, bookingDate });
};
