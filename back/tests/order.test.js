process.env.NODE_ENV = 'test';

let server = require('../');

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjlhZDBjYjdjMGY1NTkwMmY5N2RjNTI0NWE4ZTc5NzFmMThkOWM3NjYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vY29uc3RydXlvLWNvZGluZy1jaGFsbGVuZ2UiLCJhdWQiOiJjb25zdHJ1eW8tY29kaW5nLWNoYWxsZW5nZSIsImF1dGhfdGltZSI6MTYwNjMwNTc3OSwidXNlcl9pZCI6IjVpRW0xSHZJeHViTGFpS080eWowTnBtdnEwRjIiLCJzdWIiOiI1aUVtMUh2SXh1YkxhaUtPNHlqME5wbXZxMEYyIiwiaWF0IjoxNjA2MzA1Nzc5LCJleHAiOjE2MDYzMDkzNzksImVtYWlsIjoiY29kaW5nLWNoYWxsZW5nZUBjb25zdHJ1eW8uZGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiY29kaW5nLWNoYWxsZW5nZUBjb25zdHJ1eW8uZGUiXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.kwBagl4HIO-jdKgox04bGs1WEdPWL0ulnnQPV6XYiPE-RKZZdBQH81ouyc72gShAibZ639aY5z7RvvIiw4LVfvC0Rtozji8DnkIty7p8sjK6oVTId_0HR_Nv7tbVqiWUtZaNyoFExF3YGpVlxi3SqmeLglND0hHpTzUrqK8xfesnvxfRPiOBAwV2blipm462fgXsUzKmrORuzLCE1MgblDz6Zz6KDz7pw0__dM0Jf79BsXr4-yi3PPJt6Uy0Y1JyqVckf2pl9eisJMS1nnu-Mc-kzb7Rr_ebOvokmikxBEsnSFtnAptUDImwFLt5ja9-yWdD_4xPpTLvTS1k_N3jrg';

describe('Orders', () => {
    describe('/POST orders', () => {
        const order = {
            "title": "mocha test",
            "bookingDate": 1606243285378,
            "address": {
                "street": "test street",
                "zip": "28374",
                "country": "test country",
                "city": "test city"
            },
            "customer": {
                "name": "HA",
                "email": "ha@ha.com",
                "phone": "847562778291"
            }
        };
        it('should throw authentication error when the token is not presented in the header', (done) => {
            chai.request(server)
                .post('/orders')
                .send(order)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('message');
                    done();
                });
        });

        it('should create an order', (done) => {
            chai.request(server)
                .post('/orders')
                .set({ Authorization: `Bearer ${token}` })
                .send(order)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('message');
                    done();
                });
        });
    });

    describe('/PUT orders', () => {
        const order = {
            "title": "HA title updated12",
            "bookingDate": 1606244983939
        };
        const id = 'CiQyrciethnvrnJ2o4dd';
        it('should update order', (done) => {
            chai.request(server)
                .put(`/orders/${id}`)
                .set({ Authorization: `Bearer ${token}` })
                .send(order)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('message');
                    done();
                });
        })
    })
});
