const { body } = require('express-validator');

exports.createOrderValidation = [
    body('title').notEmpty(),
    body('bookingDate').notEmpty().isInt(),
    body('address.city').notEmpty().isString(),
    body('address.country').notEmpty().isString(),
    body('address.street').notEmpty().isString(),
    body('address.zip').notEmpty().isString(),
    body('customer.email').isEmail(),
    body('customer.name').notEmpty().isString(),
    body('customer.phone').notEmpty().isString(),
];

exports.editOrderValidation = [
    body('title').notEmpty(),
    body('bookingDate').notEmpty().isInt(),
];
