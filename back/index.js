const express = require('express');
const app = express();

const { initializeFirebase } = require('./services/firebase');
initializeFirebase();

const routes = require('./routes');
const bodyParser = require('body-parser');
const cors = require('cors');


app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(routes);

app.get('/', (req, res) => {
    res.json({ status: 'Success' });
});

const port = process.env.PORT || 3000;
app.listen(port);

module.exports = app;
