const router = require('express').Router();
const OrderController = require('../controllers/order');
const validator = require('../middlewares/validator');
const { createOrderValidation, editOrderValidation } = require('../validation/order');

router.post('/', [createOrderValidation, validator], OrderController.createOrder);
router.put('/:orderId', [editOrderValidation, validator], OrderController.editOrder);

module.exports = router;
