const router = require('express').Router();
const ordersRouter = require('./order');
const auth = require('../middlewares/auth');

router.use('/orders', auth, ordersRouter);

module.exports = router;