const { validationResult } = require('express-validator');

function validator(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ status: 'fail', errors: errors.array() });
    }
    return next();
}

module.exports = validator;
