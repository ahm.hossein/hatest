const admin = require('firebase-admin');

function auth(req, res, next) {
    if (process.env.ACCEPT_ALL) {
        return next();
    }
    const token = req.header('Authorization');
    if (token) {
        const idToken = token.replace('Bearer', '').trim();
        return admin
            .auth()
            .verifyIdToken(idToken)
            .then(() => {
                return next();
            })
            .catch(() => {
                return res.status(403).json({ status: 'fail', message: 'Unauthorized' });
            })
    }
    return res.status(403).json({ status: 'fail', message: 'Unauthorized' });
}

module.exports = auth;
