const admin = require('firebase-admin');
const serviceAccount = require('../serviceAccountKey.json');

exports.initializeFirebase = () => {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: 'https://construyo-coding-challenge.firebaseio.com'
    });
};
