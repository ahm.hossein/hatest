const { validationResult } = require('express-validator');
const OrderModel = require('../models/order');

exports.createOrder = async (req, res) => {
    try {
        const { title, bookingDate, address, customer } = req.body;
        await OrderModel.createOrder(title, bookingDate, address, customer);
        return res.status(200).json({ status: 'success', message: 'Order has been created.' });
    } catch (e) {
        return res.status(400).json({ status: 'fail', error: e.message });
    }
};

exports.editOrder = async (req, res) => {
    try {
        const { orderId } = req.params;
        const { title, bookingDate } = req.body;
        await OrderModel.editOrder(orderId, title, bookingDate);
        return res.status(200).json({ status: 'success', message: 'Order has been updated.' });
    } catch (e) {
        return res.status(400).json({ status: 'fail', error: e.message });
    }
};
