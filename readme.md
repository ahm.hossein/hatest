# Overview

The project is in two main directories.

  - front
  - back

### Installation

Install the front-end project dependencies by running:

```sh
$ cd front
$ yarn install
$ yarn start
```

and for the back-end project run:

```sh
$ cd back
$ yarn install
$ yarn start
``` 

# Note
### By default the ```/orders``` routes are protected by checking the ```jwt token``` in the header. if you don't want it to validate the token, add the ```ACCEPT_ALL=true``` as environment variable to the ```start``` script.