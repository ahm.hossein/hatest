import { ComponentProps } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { RedirectUserType } from 'types';
import * as ROUTES from 'constants/routes';

type RedirectType = ComponentProps<typeof Route> & RedirectUserType;
type ProtectedRouteType = ComponentProps<typeof Route> & Omit<RedirectUserType, 'loggedInPath'>;

export function RedirectUser({ user, loggedInPath, children, ...rest }: RedirectType) {
    return (
        <Route
            {...rest}
            exact
            render={() => {
                if (!user) {
                    return children;
                }
                if (user) {
                    return <Redirect to={{ pathname: loggedInPath }} />;
                }
            }}
        />
    );
}

export function ProtectedRoute({ user, children, ...rest }: ProtectedRouteType) {
    return (
        <Route
            {...rest}
            exact
            render={({ location }) => {
                if (user) {
                    return children;
                }
                if (!user) {
                    return <Redirect to={{ pathname: ROUTES.LOGIN, state: { from: location } }} />;
                }
            }}
        />
    );
}
