import { createContext } from 'react';
import { FireApp } from 'types';
import { firebase } from 'lib/firebase';

export const FirebaseContext = createContext<FireApp>({ firebase });
