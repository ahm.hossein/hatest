import { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from 'context/firebase';
import { Order } from 'types';

export default function useOrder(id: string) {
    const [order, setOrder] = useState<Order | null>(null);

    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        firebase
            .firestore()
            .collection('orders')
            .doc(id)
            .get()
            .then(snapshot => {
                const data = snapshot.data() as Order;
                setOrder({
                    ...data,
                    id: snapshot.id,
                });
            })
    }, [id, firebase]);
    return order;
}
