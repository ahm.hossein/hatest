import { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from 'context/firebase';
import Firebase from 'firebase/app';

export default function useAuthListener() {
    const persistedUser = localStorage.getItem('authUser');
    const localUserObj = persistedUser ? JSON.parse(persistedUser!) as Firebase.User : null;
    const [user, setUser] = useState<Firebase.User | null>(localUserObj);
    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        const listener = firebase.auth().onAuthStateChanged((authUser) => {
            authUser?.getIdToken().then((token) => {
                localStorage.setItem('token', token);
            })
            if (authUser) {
                localStorage.setItem('authUser', JSON.stringify(authUser));
                setUser(authUser);
            } else {
                localStorage.removeItem('authUser');
                localStorage.removeItem('token');
                setUser(null);
            }
        });
        return () => listener();
    }, [firebase]);
    return user;
}
