import { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from 'context/firebase';
import { Order } from 'types';

export default function useOrders() {
    const [orders, setOrders] = useState<Order[]>([]);
    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        firebase
            .firestore()
            .collection('orders')
            .get()
            .then(snapshot => {
                const allOrders = snapshot.docs.map(order => {
                    const data = order.data() as Order;
                    const bookingDate = typeof data.bookingDate === 'object'
                        ? data.bookingDate.seconds
                        : data.bookingDate;
                    const newOrder = {
                        ...data,
                        id: order.id,
                        bookingDate,
                    };
                    return newOrder;
                });
                setOrders(allOrders as Order[]);
            })
    }, [firebase]);
    return orders;
}
