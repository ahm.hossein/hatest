import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
    html, body {
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        background: url(/images/background.jpg);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        color: #333;
        font-size: 16px;
        height: 100%;
    }
    #root {
        height: 100%;
    }
`;
