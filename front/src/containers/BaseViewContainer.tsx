import { Background } from 'components';
import { ReactNode, useContext } from 'react';
import { FirebaseContext } from 'context/firebase';

type Props = {
    children: ReactNode;
    title: string;
}

export default function BaseViewContainer({ children, title }: Props) {
    const { firebase } = useContext(FirebaseContext);

    const logOut = () => {
        firebase
            .auth()
            .signOut()
            .catch(console.log);
    };

    return (
        <Background>
            <Background.Header>
                <Background.HeaderTitle>
                    {title}
                </Background.HeaderTitle>
                <Background.Button onClick={logOut}>
                    Logout
                </Background.Button>
            </Background.Header>
            <Background.Body>
                {children}
            </Background.Body>
        </Background>
    );
}
