import moment from 'moment';
import { ListView } from 'components';
import { useOrders } from 'hooks';

export default function OrdersContainer() {
    const orders = useOrders();
    return (
        <ListView>
            <ListView.Header>
                <ListView.Item>Title</ListView.Item>
                <ListView.Item>Booking Date</ListView.Item>
                <ListView.Item>Address</ListView.Item>
                <ListView.Item>Customer</ListView.Item>
                <ListView.Item>Actions</ListView.Item>
            </ListView.Header>
            {
                orders.length > 0 && orders.map(item => {
                    const address = item.address ? Object.values(item.address).join(', ') : '-';
                    const date = item.bookingDate ? moment(item.bookingDate).format('YYYY-MM-DD hh:mm') : '-';
                    return (
                        <ListView.Row key={item.id}>
                            <ListView.Item>{item.title}</ListView.Item>
                            <ListView.Item>{date}</ListView.Item>
                            <ListView.Item>{address}</ListView.Item>
                            <ListView.Item>{item.customer?.name || '-'}</ListView.Item>
                            <ListView.Item>
                                <ListView.Button to={`/orders/${item.id}`}>Edit</ListView.Button>
                            </ListView.Item>
                        </ListView.Row>
                    )
                })
            }
        </ListView>
    );
}
