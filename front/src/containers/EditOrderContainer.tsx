import { useState } from 'react';
import moment from 'moment';
import { Form } from 'components';
import { EditOrderType, Timestamp } from "types";

type Props = {
    onSubmit: (data: EditOrderType) => void;
    title?: string;
    bookingDate?: number | Timestamp;
    error?: string;
    message?: string;
}

export default function EditOrderContainer({ onSubmit, title = '', bookingDate, error, message }: Props) {
    const [formTitle, setFormTitle] = useState(title);
    const [formBookingDate, setFormBookingDate] = useState(
        bookingDate && typeof bookingDate === 'object' ? bookingDate.seconds : bookingDate
    );

    const isInvalid = formTitle === '' || formBookingDate === 0;

    const onBookingDateChanged = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
        const date = new Date(target.value).getTime();
        setFormBookingDate(date);
    };

    const onFormSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        onSubmit({ title: formTitle, bookingDate: formBookingDate });
    }

    return (
        <Form onSubmit={onFormSubmit}>
            {error && <Form.Error>{error}</Form.Error>}
            {message && <Form.Success>{message}</Form.Success>}
            <Form.Input
                type='text'
                placeholder="Title"
                name='title'
                required
                value={formTitle}
                onChange={({ target }: React.ChangeEvent<HTMLInputElement>) => setFormTitle(target.value)}
            />
            <Form.Input
                type='date'
                required
                placeholder="Booking Date"
                value={formBookingDate ? moment(formBookingDate).format('YYYY-MM-DD') : ''}
                onChange={onBookingDateChanged}
            />
            <Form.Button disabled={isInvalid} type='submit'>Login</Form.Button>
        </Form>
    );
}