import Firebase from 'firebase/app';
import { ReactNode } from "react";

declare type GeneralProps = {
    children: ReactNode;
}

declare type FireApp = {
    firebase: Firebase.app.App
}

declare type RedirectUserType = {
    user: Firebase.User | null;
    loggedInPath: string;
    children: ReactNode;
}

declare type Order = {
    address?: Address;
    bookingDate?: number | Timestamp;
    customer?: Customer;
    title: string;
    id: string;
};

declare type Timestamp = {
    seconds: number;
}

declare type Customer = {
    email: string;
    name: string;
    phone: string;
}

declare type Address = {
    city: string;
    country: string;
    street: string;
    zip: string;
}

declare type EditOrderType = Pick<Order, 'title' & 'bookingDate'>;
