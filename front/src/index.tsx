import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'normalize.css';
import { FirebaseContext } from './context/firebase';
import { firebase } from './lib/firebase';
import { GlobalStyles } from './globalStyles';

ReactDOM.render(
  <React.StrictMode>
    <FirebaseContext.Provider value={{ firebase }}>
      <>
        <GlobalStyles />
        <App />
      </>
    </FirebaseContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
