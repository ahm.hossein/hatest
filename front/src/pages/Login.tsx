import { useHistory } from 'react-router-dom';
import { FirebaseContext } from 'context/firebase';
import { useState, useContext } from 'react';
import { Form } from '../components';
import * as ROUTES from 'constants/routes';

export default function Login() {
    const history = useHistory();
    const { firebase } = useContext(FirebaseContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('')

    // basic validation
    const isInvalid = password === '' || email === '';

    const signIn = (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                history.push(ROUTES.DASHBOARD);
            })
            .catch(e => {
                setEmail('');
                setPassword('');
                setError(e.message);
            })
    }

    return (
        <Form onSubmit={signIn}>
            {error && <Form.Error>{error}</Form.Error>}
            <Form.Input
                type='email'
                placeholder="E-mail"
                value={email}
                onChange={({ target }: React.ChangeEvent<HTMLInputElement>) => setEmail(target.value)}
            />
            <Form.Input
                type='password'
                autoComplete="off"
                placeholder="Password"
                value={password}
                onChange={({ target }: React.ChangeEvent<HTMLInputElement>) => setPassword(target.value)}
            />
            <Form.Button disabled={isInvalid} type='submit'>Login</Form.Button>
        </Form>
    );
}
