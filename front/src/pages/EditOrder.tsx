import { useState } from 'react';
import { useParams } from 'react-router-dom';
import { BaseViewContainer, EditOrderContainer } from "containers";
import { useOrder } from 'hooks';
import { EditOrderType } from 'types';
import Api from 'lib/api';

type ParamsType = {
    id: string;
}

export default function EditOrder() {
    const { id } = useParams<ParamsType>();
    const order = useOrder(id);

    const [error, setError] = useState('');
    const [message, setMessage] = useState('');

    const onSubmit = async (data: EditOrderType) => {
        const api = new Api();
        try {
            const response = await api.req('PUT', `/orders/${id}`, data);
            setMessage(response);
        } catch (e) {
            setError(e.message);
        }
    };

    return (
        <BaseViewContainer title="Edit Order">
            {/*I could use <Suspense /> too, but for the simplicity I just checked if the order object fetched.*/}
            {
                order && (
                    <EditOrderContainer
                        error={error}
                        message={message}
                        title={order?.title}
                        bookingDate={order?.bookingDate}
                        onSubmit={onSubmit}
                    />
                )
            }
        </BaseViewContainer>
    );
}
