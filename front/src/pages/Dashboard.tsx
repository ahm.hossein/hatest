import { BaseViewContainer, OrdersContainer } from "containers";


export default function Dashboard() {
    return (
        <BaseViewContainer title="Orders List">
            <OrdersContainer />
        </BaseViewContainer>
    );
}
