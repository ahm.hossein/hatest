import axios, { AxiosInstance } from 'axios';

type Method = 'POST' | 'PUT' | 'GET' | 'DELETE';

type ValidationError = {
    param: string;
    msg?: string;
    location?: string;
}

export default class Api {

    private instance: AxiosInstance;

    constructor() {
        const token = localStorage.getItem('token');
        this.instance = axios.create({
            baseURL: process.env.REACT_APP_API_URL,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        });
    }

    async req(method: Method, url: string, data?: any): Promise<string> {
        try {
            const response = await this.instance({
                method,
                url,
                data,
            });
            return response.data.message;
        } catch (e) {
            const response = e.response;
            if (response && response.status === 422 && response.data.errors) {
                const errorMessage = this.processValidationError(response.data.errors);
                throw new Error(errorMessage);
            }
            throw new Error(e);
        }

    }

    private processValidationError(errors: any): string {
        const validationErrors = errors;
        return 'Invalid value for ' + validationErrors.map(({ param }: ValidationError) => param).join(', ');
    }

}
