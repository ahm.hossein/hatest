import { BrowserRouter as Router } from 'react-router-dom';
import * as ROUTES from 'constants/routes';
import { RedirectUser, ProtectedRoute } from 'helpers/routes';
import { Login, Dashboard, EditOrder } from './pages';
import { useAuthListener } from 'hooks';

function App() {
  const user = useAuthListener();
  return (
    <Router>
      <RedirectUser user={user} loggedInPath={ROUTES.DASHBOARD} path={ROUTES.LOGIN}>
        <Login />
      </RedirectUser>
      <ProtectedRoute user={user} path={ROUTES.DASHBOARD}>
        <Dashboard />
      </ProtectedRoute>
      <ProtectedRoute user={user} path={ROUTES.EDIT_ORDER}>
        <EditOrder />
      </ProtectedRoute>
    </Router>
  );
}

export default App;
