import { View, Header, Body, Button, HeaderTitle } from './styles';
import { GeneralProps } from "types";
import { ComponentProps } from 'react';

export default function Background({ children, ...restProps }: GeneralProps) {
    return <View {...restProps}>{children}</View>;
}

Background.Header = function BackgroundHeader({ children, ...restProps }: GeneralProps) {
    return <Header {...restProps}>{children}</Header>;
}

Background.HeaderTitle = function BackgroundHeaderTitle({ children, ...restProps }: GeneralProps) {
    return <HeaderTitle {...restProps}>{children}</HeaderTitle>;
}

Background.Body = function BackgroundBody({ children, ...restProps }: GeneralProps) {
    return <Body {...restProps}>{children}</Body>;
}

type BackgroundButtonType = ComponentProps<typeof Button> & GeneralProps;

Background.Button = function BackgroundButton({ children, ...restProps }: BackgroundButtonType) {
    return <Button {...restProps}>{children}</Button>;
}
