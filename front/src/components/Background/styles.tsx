import styled from 'styled-components';

export const View = styled.div`
    background-color: white;
    width: 85%;
    min-height: 85%;
    margin: 20px auto;
    display: flex;
    border-radius: 10px;
    flex-direction: column;
`;

export const Body = styled.div`
    padding: 16px;
    display: flex;
    height: 100%;
`;

export const Header = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: #441A6F;
    height: 70px;
    padding: 10px 16px;
    border-radius: 10px 10px 0px 0px;
`;

export const Button = styled.button`
    background: white;
    min-width: 140px;
    height: 40px;
    border: 0px;
    border-radius: 20px;
    cursor: pointer;
`;


export const HeaderTitle = styled.h1`
    color: white;
`;