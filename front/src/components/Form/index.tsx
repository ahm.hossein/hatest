import { ComponentProps } from "react";
import { GeneralProps } from "../../types";
import { Container, Input, Button, Message } from './styles';

type FormProps = ComponentProps<typeof Container> & GeneralProps;
type ButtonProps = ComponentProps<typeof Button> & GeneralProps;
type ErrorProps = ComponentProps<typeof Message> & GeneralProps;

export default function Form({ children, ...restProps }: FormProps) {
    return <Container {...restProps}>{children}</Container>;
}

Form.Input = function FormInput({ ...restProps }) {
    return <Input {...restProps} />;
}

Form.Button = function FormButton({ children, ...restProps }: ButtonProps) {
    return <Button {...restProps}>{children}</Button>;
}

Form.Error = function FormError({ children, ...restProps }: ErrorProps) {
    return <Message isError {...restProps}>{children}</Message>
}

Form.Success = function FormSuccess({ children, ...restProps }: ErrorProps) {
    return <Message {...restProps}>{children}</Message>
}
