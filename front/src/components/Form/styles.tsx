import styled from 'styled-components';

type MessageProps = {
    isError: boolean;
}

export const Container = styled.form`
    display: flex;
    flex-direction: column;
    max-width: 500px;
    border-radius: 25px;
    background-color: white;
    margin: 50px auto;
    padding: 60px 45px;
    max-height: 400px;
`;

export const Input = styled.input`
    height: 56px;
    padding: 12px;
    outline: 0;
    border: 1px solid #ddd;
    border-radius: 4px;
    background: #fff;
    margin-bottom: 20px;
`;

export const Button = styled.button`
    background: #441A6F;
    border-radius: 45px;
    margin: 28px 0 12px;
    padding: 16px;
    border: 0;
    color: white;
    cursor: pointer;

    &:disabled {
        opacity: 0.5;
    }
`;

export const Message = styled.p`
    margin: 10px auto;
    background: #333;
    font-size: 14px;
    color: ${({ isError }: MessageProps) => isError ? '#ff0033' : '#6cc070'};
    padding: 10px 15px;
    text-align: center;
    line-height: 2em;
`;
