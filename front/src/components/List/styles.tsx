import styled from 'styled-components';
import { Link } from 'react-router-dom';

type RowProps = {
    bold?: boolean;
    dark?: boolean;
}

export const List = styled.ul`
    width: 100%;
    margin: 0;
    padding: 0;
    list-style-type: none;
`;

export const Row = styled.li`
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: ${({ dark }: RowProps) => dark ? '#eee' : '#efefef'};
    font-size: 14px;
    font-weight: ${({ bold }: RowProps) => bold ? 'bold' : 'normal'};
    box-sizing: border-box;
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);

    &:first-child {
        border-radius: 3px 3px 0px 0px;
    }
    
    &:last-child {
        border-radius: 0px 0px 3px 3px;
        border: 0px;
    }
`;

export const Item = styled.div`
    border-right: 1px solid rgba(0, 0, 0, 0.05);
    padding: 16px;
    display: flex;
    justify-content: center;

    &:nth-child(1) {
        flex-basis: 15%;
    }
    
    &:nth-child(2) {
        flex-basis: 15%;
    }
    
    &:nth-child(3) {
        flex-basis: 40%;
    }
    
    &:nth-child(4) {
        flex-basis: 20%;
    }

    &:last-child {
        flex-basis: 10%;
        border: 0px;
    }
`;

export const Button = styled(Link)`
    display: flex;
    align-items: center;
    text-decoration: none;
    color: #441A6F;
    cursor: pointer;
    padding: 0;
    margin: 0;
    border: 0;
    background: transparent;
    font-weight: bold;
`;
