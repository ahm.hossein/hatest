import { ComponentProps } from 'react';
import { List, Item, Row, Button } from './styles';
import { GeneralProps } from "types";

type ListViewProps = ComponentProps<typeof List> & GeneralProps;
type ListItemProps = ComponentProps<typeof Item> & GeneralProps;
type ListRowProps = ComponentProps<typeof Row> & GeneralProps;
type ListButtonProps = ComponentProps<typeof Button> & GeneralProps;

export default function ListView({ children, ...restProps }: ListViewProps) {
    return <List {...restProps}>{children}</List>;
}

ListView.Header = function ListHeader({ children, ...restProps }: GeneralProps) {
    return <Row children={children} bold {...restProps} dark />;
}

ListView.Item = function ListItem({ children, ...restProps }: ListItemProps) {
    return <Item {...restProps}>{children}</Item>;
}

ListView.Row = function ListRow({ children, ...restProps }: ListRowProps) {
    return <Row {...restProps}>{children}</Row>;
}

ListView.Button = function ListButton({ children, ...restProps }: ListButtonProps) {
    return <Button {...restProps}>{children}</Button>;
}